//
//  ViewController.swift
//  rotateImage
//
//  Created by Nivendru on 24/02/17.
//  Copyright © 2017 Nivendru. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var rotateImgView: UIImageView!
    var issstart : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        (UIApplication.shared.delegate as! AppDelegate).rotate(rotateImgView)
        (UIApplication.shared.delegate as! AppDelegate).rotateimageview = rotateImgView
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func stop(_ sender: Any) {
        if(issstart == false)
        {
        rotateImgView.layer.removeAllAnimations()
            issstart = true
            (sender as! UIButton).setTitle("Start", for: .normal)
        }
        else
        {
            (UIApplication.shared.delegate as! AppDelegate).rotate(rotateImgView)
            issstart = false
            (sender as! UIButton).setTitle("Stop", for: .normal)
        }
       
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
//    - (void) runSpinAnimationOnView:(UIView*)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat
//    {
//    CABasicAnimation* rotationAnimation;
//    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
//    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * rotations * duration ];
//    rotationAnimation.duration = duration;
//    rotationAnimation.cumulative = YES;
//    rotationAnimation.repeatCount = repeat;
//    
//    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
//    }
}

